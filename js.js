let
  i = 0,
  play = true,
//   音乐播放状态
   muted = false,
// 静音状态
   loop = false,
// 单曲循环
  total_min = 0,total_sec = 0,play_min = 0,play_sec = 0, 
// 输出音乐时长的变量
  audio = document.getElementById('audio'),
//   音乐
  img = document.querySelector('.img'),
//   唱片
  range = document.getElementById('line'),
//   滑块
Mc = [
    "Mc/01.mp3",
    "Mc/02.mp3",
    "Mc/03.mp3",
    "Mc/04.mp3",
    "Mc/05.mp3"
],
img_list = [
    "Mc/001.jpg",
    "Mc/002.jpg",
    "Mc/003.jpg",
    "Mc/004.jpg",
    "Mc/005.jpg"
],

prev_btn = document.getElementById('prev-btn'),
// 上一首按钮
play_btn = document.getElementById('play-btn'),
// 播放按钮
next_btn = document.getElementById('next-btn'),
// 下一首按钮
loop_btn = document.getElementById('loop-btn'),
// 单曲循环按钮
random_btn = document.getElementById('random-btn'),
// 随机循环按钮
mute_btn = document.getElementById('mute-btn'),
// 静音按钮
volume_btn = document.getElementById('volume-btn'),
// 音量按钮
volume = document.getElementById('volume');
// 音量滑块
start_min = document.querySelector('.start-time .min'),
// 播放分钟
start_sec = document.querySelector('.start-time .sec'),
// 播放秒钟
end_min = document.querySelector('.end-time .min'),
// 音乐总分钟
end_sec = document.querySelector('.end-time .sec');
// 播放总秒钟


// 播放开始
play_btn.onclick = function(){
  if(play){
    // 判断音乐播放状态是否为false
    audio.play();
    // 音乐播放
    play =false;
    // 将音乐播放状态设置为false
    this.classList.remove('icon-play');
    // 给当前按钮，增加样式
    this.classList.add('icon-pause');
    // 给当前按钮，删除样式
    img.classList.remove('paused');
    //给唱片图片 删除样式
    img.classList.add('rotate');
    // 给唱片图片 增加类样式
  }else{
    // 如果是音乐播放状态是否为false
    audio.pause();
    // 音乐暂停
    play = true;
    // 将音乐播放状态设置为true
    this.classList.remove('icon-pause');
    // 给当前按钮，删除样式
    this.classList.add('icon-play');
    // 给当前按钮，增加样式
    img.classList.add('paused');
    // 给唱片图片 增加类样式
  }
};
// 上一首
prev_btn.onclick = function (){
  i -= 1;
  if (i < 0) {
    i = 4;
  }
  // 如果变量i的值小于0，直接赋值i的值为4
  audio.src =Mc[i];
  // 音乐列表进行更新
  audio.play();
  // 进行播放音乐
  play = false;
  // 音乐状态为false
  img.src = img_list[i];
  // 唱片图片列表进行更新
  img.classList.add('rotate');
  // 给唱片增加样式
  play_btn.classList.remove('icon-play');
  // 播放按钮的样式
  play_btn.classList.add('icon-pause');
  // 增加暂停按钮的样式
}
// 下一首 
next_btn.onclick = function (){
 i += 1;

 if (i > 4) {

  i = 0;
 }
 audio.src = Mc[i];

 img.src =img_list[i];

 audio.play();

 play = false;

 img.classList.add('rotate');

 play_btn.classList.remove('icon-play');

 play_btn.classList.add('icon-pause');
}
// 静音
mute_btn.onclick = function () {
  if (muted){
 audio.muted = false;
 muted = false;
 this.classList.remove('flash');
} else {
  audio.muted = true;
  muted = true;
  this.classList.add('flash')
    }
}
// 音量按钮
volume_btn.onclick = function () {
 volume.style.opacity =1;
 volume.onchange = function(){
   let value =this.value / 100;
   console.log(value);
   audio.volume = value;
 }

 volume.onmouseout = function (){
  volume.style.opacity = 0;
 }
}

audio.onplaying = function (){
  let total_time = this.duration;
  // 当前音乐时长(秒)
      
      //  当前音乐播放的当前位置(秒)
  total_min=Math.floor(total_time/60);
  if(total_min<10)total_min='0'+total_min;
  end_min.innerHTML=total_min;

  total_sec=Math.floor(total_time%60);
  if(total_sec<10)total_sec='0'+total_sec;
  end_sec.textContent=total_sec;
  console.log(total_sec);


// 音乐进度条
  setInterval(function(){
    let current_time = audio.currentTime;
    play_sec=Math.floor(current_time % 60);
    if(play_sec<10)play_sec='0'+play_sec;
    start_sec.innerHTML=play_sec;
    // console.log(play_sec);

    play_min=Math.floor(current_time / 60);
    if(play_min<10)play_min='0'+play_min;
    start_min.innerHTML=play_min;

    range.onchange=function(){
      console.log(range.value);
      audio.currentTime=total_time*range.value /100;

      this.setAttribute('value',total_time*range.value /100);
    }
 
    range.value= current_time / total_time * 100;
// 当前播放秒/当前音乐总时长*100

  },500);

}

